package com.okalman.locator;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.net.wifi.ScanResult;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.okalman.locator.asynctasks.AsyncExportDb;
import com.okalman.locator.asynctasks.AsyncFingerprint;
import com.okalman.locator.asynctasks.AsyncQueryDBMeasuredRecords;
import com.okalman.locator.db.MeasureData;
import com.okalman.locator.db.MeasureRecord;
import com.okalman.locator.listeners.DbQueryListener;
import com.okalman.locator.utils.Constants;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.net.URL;
import java.util.List;
import java.util.Objects;

import rapid.decoder.BitmapDecoder;
import uk.co.senab.photoview.PhotoViewAttacher;

public class MainActivity extends AppCompatActivity implements PhotoViewAttacher.OnPhotoTapListener {

    ImageView imageMap=null;
    TextView textInfo=null;
    PhotoViewAttacher photoViewAttacher=null;
    AsyncTask<Void, Long, List<List<ScanResult>>> collectWifi = null;
    File temp = null;
    URL website = null;
    int floor =0;
    Bitmap bm;
    ViewGroup vg = null;
    String link="http://indica.mendelu.cz/mapa-budovy/mistnost-Q1.20/velikost-900.png";
    DbQueryListener dbListener= null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(this.getClass().getSimpleName(), "onCreate called");
        if(this.getIntent().getStringExtra("link")!=null) {
            link = this.getIntent().getStringExtra("link");
            floor = this.getIntent().getIntExtra("floor",0);
        }
        setContentView(R.layout.activity_main);
        imageMap = (ImageView) findViewById(R.id.image_map);
        textInfo = (TextView) findViewById(R.id.text_info);
        Toolbar myToolbar = (Toolbar) findViewById(R.id.toolbar);
        dbListener = new DbQueryListener() {
            @Override
            public void onDataReady(Object object) {
                Canvas canvas=null;
                if(object == null) return;
                if(object instanceof List){
                    List list = (List)object;
                    for (Object o: list){
                        if(o instanceof MeasureRecord){
                            if(canvas == null){
                                canvas = setImage();
                            }
                            highlitTile(canvas,((MeasureRecord) o).getTile());
                        }
                    }

                }

            }
        };

        setSupportActionBar(myToolbar);
        try {
            website = new URL(link);
            temp = File.createTempFile("prefix", ".png", this.getCacheDir());

            new AsyncTask<Object, Void, MainActivity>() {
                @Override
                protected MainActivity doInBackground(Object... params) {
                    try {
                        FileUtils.copyURLToFile((URL) params[0], (File) params[1]);
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                    Log.d(this.getClass().getSimpleName(), "image loaded");
                    return (MainActivity)params[2];
                }
                @Override
                protected void onPostExecute(MainActivity activty){
                    activty.setImage();
                    new AsyncQueryDBMeasuredRecords(activty,dbListener).execute();
                }

            }.execute(new Object[]{website, temp, this});

        }catch (Exception e){
            e.printStackTrace();
        }

    }
    public Canvas setImage(){
        imageMap.setImageResource(0);
        Log.d(this.getClass().getSimpleName(), "setting image");
        if(bm==null) {
            bm = BitmapDecoder.from(temp.getAbsolutePath()).mutable().decode();
        }
        Bitmap empty = Bitmap.createBitmap(bm.getWidth(), bm.getHeight(), Bitmap.Config.ARGB_8888);
        empty.setHasAlpha(true);
        Bitmap tmpBitmap= createMatrixOverLay(bm);
        Canvas canvas = new Canvas(empty);
        canvas.drawBitmap(bm, new Matrix(), null);
        canvas.drawBitmap(tmpBitmap, new Matrix(), null);

        //thohle vypocita souradnice kam umisti ctverecek kde se prave nachazime
//        int xPos = (146 % Constants.numberOfSquaresInLine)*(bm.getWidth()/Constants.numberOfSquaresInLine);
//        int yPos = (int)(146/((float)Constants.numberOfSquaresInLine))*(bm.getWidth()/Constants.numberOfSquaresInLine);
//        canvas.drawBitmap(createOverlay2(bm), xPos, yPos, null);
        ////
        imageMap.setImageBitmap(empty);
        photoViewAttacher = new PhotoViewAttacher(imageMap);
        photoViewAttacher.setOnPhotoTapListener(this);
        return canvas;
    }

    Bitmap createMatrixOverLay(Bitmap bm){
        int squareSize=bm.getWidth()/ Constants.numberOfSquaresInLine;
        int width=bm.getWidth();
        int height=bm.getHeight();
        Bitmap bmOverlay = Bitmap.createBitmap(bm.getWidth(), bm.getHeight(), Bitmap.Config.ARGB_8888);
        bmOverlay.setHasAlpha(true);
        int[] pixels = new int[bm.getWidth() * bm.getHeight()];
        for(int i=0; i<pixels.length; i++){
            if(i%squareSize==0){
                pixels[i]=0x808000FF;
            }
        }
        for(int i=0; i<width; i++){
            int j=0;
            while(j<height-squareSize){
                j=j+squareSize;
                pixels[j*width+i]=0x808000FF;
            }
        }
        bmOverlay.setPixels(pixels, 0, bmOverlay.getWidth(), 0, 0, bmOverlay.getWidth(), bmOverlay.getHeight());
        return bmOverlay;
    }

    public void highlitTile(Canvas canvas, int tile){
        int xPos = (tile % Constants.numberOfSquaresInLine)*(bm.getWidth()/Constants.numberOfSquaresInLine);
        int yPos = (int)(tile/((float)Constants.numberOfSquaresInLine))*(bm.getWidth()/Constants.numberOfSquaresInLine);
        canvas.drawBitmap(createOverlay2(bm), xPos, yPos, null);
    }

    //thohle vytvori ctverecek kde se asi nachazime
    Bitmap createOverlay2(Bitmap bm){


        int squareSize=bm.getWidth()/ Constants.numberOfSquaresInLine;
        int width=bm.getWidth();
        int height=bm.getHeight();
        Bitmap bmOverlay = Bitmap.createBitmap(bm.getWidth()/Constants.numberOfSquaresInLine, bm.getHeight()/Constants.numberOfSquaresInLine, Bitmap.Config.ARGB_8888);
        bmOverlay.setHasAlpha(true);
        int[] pixels = new int[bmOverlay.getWidth() * bmOverlay.getHeight()];
        for(int i=0; i<pixels.length; i++){
            pixels[i]=0xFFFF0000;
        }
        bmOverlay.setPixels(pixels, 0, bmOverlay.getWidth(), 0, 0, bmOverlay.getWidth(), bmOverlay.getHeight());
        return bmOverlay;
    }



    @Override
    public void onPhotoTap(View view, float x, float y) {
        int iH=((ImageView)view).getDrawable().getIntrinsicHeight();
        int iW=((ImageView)view).getDrawable().getIntrinsicWidth();
        int xreal = Math.round(x * iW);
        int yreal = Math.round(y * iH);
        Toast.makeText(this, "Tapped on x:"+xreal + "; y:"+yreal+";" ,Toast.LENGTH_SHORT).show();
        collectWifi = new AsyncFingerprint(x,y, floor,this,Constants.COLLECTING_TIMEOUT, dbListener);
        collectWifi.execute();
        Log.d(this.getLocalClassName(), "TAPPED");
    }
    @Override
    public void onDestroy() {
        super.onDestroy();

        // Need to call clean-up
        photoViewAttacher.cleanup();
        imageMap.setImageResource(0);
        imageMap.invalidate();
        imageMap.refreshDrawableState();

    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        menu.add("-1.floor");
        menu.add("0.floor");
        menu.add("1.floor");
        menu.add("2.floor");
        menu.add("3.floor");
        menu.add("4.floor");
        menu.add("5.floor");
        menu.add("export");
        menu.add("localization");
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch(item.getTitle().toString()){
            case "-1.floor":{
                link = "http://indica.mendelu.cz/mapa-budovy/mistnost-Q01.48/velikost-900.png";
                floor = -1;
                break;
            }
            case "0.floor":{
                link="http://indica.mendelu.cz/mapa-budovy/mistnost-Q1.20/velikost-900.png";
                floor = 0;
                break;
            }
            case "1.floor": {
                link = "http://indica.mendelu.cz/mapa-budovy/mistnost-Q2.20/velikost-900.png";
                floor = 1;
                break;
            }case "export":{
                AsyncExportDb export = new AsyncExportDb(this);
                export.execute();
                return super.onOptionsItemSelected(item);
            }
            case "localization":{
                Intent it= new Intent(this,LocationActivity.class);
                startActivity(it,null);
                return super.onOptionsItemSelected(item);
            }

        }
        Intent it= new Intent(this,MainActivity.class);
        Bundle b = new Bundle();
        it.putExtra("link",link);
        it.putExtra("floor", floor);
        startActivity(it);
        finish();
        return super.onOptionsItemSelected(item);
    }
}
