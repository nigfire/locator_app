package com.okalman.locator.asynctasks;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.util.Log;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.okalman.locator.R;
import com.okalman.locator.db.MeasureData;
import com.okalman.locator.listeners.LocalizationListener;
import com.okalman.locator.utils.JsonAPI;


import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicHeader;

import java.io.InputStream;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by okalman on 20.3.16.
 */
public class AsyncQuery extends AsyncTask<Void, String, Integer> {
    private Context context;
    private LocalizationListener listener;
    private ProgressDialog dialog;
    public AsyncQuery(Context context, LocalizationListener listener){
        this.context=context;
        this.listener =listener;

    }

    private void setupDialog(){
        dialog = new ProgressDialog(context);
        dialog.setMax(100);
        dialog.setCancelable(false);
        dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setMessage(context.getString(R.string.scan_dialog));
        dialog.show();
    }
    @Override
    protected void onPreExecute(){
        setupDialog();
    }

    @Override
    protected void onProgressUpdate(String... s){
        if(s != null && s.length>0){
            if(dialog == null){
                setupDialog();
            }
            dialog.setMessage(s[0]);

        }

    }

    @Override
    protected Integer doInBackground(Void... params) {
        WifiManager mainWifi = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        mainWifi.startScan();

        List<List<MeasureData>> data = new ArrayList<>();
        int counter = 0;
        long ts=0;
        while(counter <4){
            List<ScanResult> scanResults = mainWifi.getScanResults();
            if(scanResults.get(0).timestamp!=ts){
                ts=scanResults.get(0).timestamp;
                List<MeasureData> dataSacan = new ArrayList<>();
                for (ScanResult scan : scanResults) {
                    dataSacan.add(new MeasureData(null, scan.SSID, scan.BSSID, scan.level));
                }
                data.add(dataSacan);
                counter++;
            }
        }
        publishProgress(new String[]{"Sending request"});

//        data.add(new MeasureData(null, "eduroam","1c:1d:86:30:6f:31",-79));
//        data.add(new MeasureData(null, "eduroam","5c:a4:8a:68:6e:41",-64));
//        data.add(new MeasureData(null, "eduroam","5c:a4:8a:92:50:b1",-59));
//        data.add(new MeasureData(null, "eduroam","88:f0:31:da:7e:51",-81));
//        data.add(new MeasureData(null, "eduroam","c0:7b:bc:cf:64:61",-82));
//        data.add(new MeasureData(null, "eduroam","c0:7b:bc:cf:71:e1",-71));
//        data.add(new MeasureData(null, "eduroam","zz:zz:zz:zz:zz:zz",0));
//        data.add(new MeasureData(null, "eduroam","zz:zz:zz:zz:zz:zz",0));
        JsonAPI request = new JsonAPI();
        request.data=data;
        ObjectMapper mapper = new ObjectMapper();
        CloseableHttpClient httpclient = HttpClients.createDefault();

        try{

            String json = mapper.writeValueAsString(request);
            Log.d(AsyncQuery.class.getSimpleName(), json);
            HttpPost httpPost = new HttpPost("https://wifi-locator2.eu-gb.mybluemix.net/LocatingServlet");
            List<NameValuePair> nvps = new ArrayList<NameValuePair>();
//            nvps.add(new BasicNameValuePair("accesskey", Constants.KEY));
            BasicHeader header= new BasicHeader("Content-type","application/json;charset=UTF-8");
            httpPost.setHeader(header);
            StringEntity entity = new StringEntity(json);
            httpPost.setEntity(entity);
//            httpPost.setEntity(new UrlEncodedFormEntity(nvps));

            HttpResponse response =httpclient.execute(httpPost);
            InputStream is=response.getEntity().getContent();
            StringWriter writer = new StringWriter();
            IOUtils.copy(is, writer, "UTF-8");
            String theString = writer.toString();
            JsonAPI responseJson=mapper.readValue(theString,JsonAPI.class);

            return Integer.valueOf(Double.valueOf(responseJson.response).intValue());
        }catch (Exception e){
            Log.e(this.getClass().getSimpleName(),e.getMessage());
            e.printStackTrace();
        }
        return null;
    }
    @Override
    protected void onPostExecute(Integer result){
        if(dialog != null){
            dialog.dismiss();
        }
        if (listener !=null){
            listener.onResultReady(result);
        }

    }




}
