package com.okalman.locator.asynctasks;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.j256.ormlite.dao.Dao;
import com.okalman.locator.R;
import com.okalman.locator.db.DatabaseHelper;
import com.okalman.locator.db.MeasureData;
import com.okalman.locator.db.MeasureRecord;
import com.okalman.locator.utils.Constants;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by okalman on 9.3.16.
 */
public class AsyncSaveToDb extends AsyncTask<Void, Void, Void> {
    float x,y ;
    AlertDialog dialog;
    Context context;
    int floor;
    List<List<ScanResult>> listOfResults;
    public AsyncSaveToDb(float x, float y,int floor, Context context, List<List<ScanResult>> listOfResults){
        this.floor=floor;
        this.context=context;
        this.listOfResults=listOfResults;
        this.x=x;
        this.y=y;
        dialog = new ProgressDialog(context);
    }



    @Override
    protected void onPreExecute(){
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setMessage(context.getString(R.string.scan_save_dialog));
        dialog.show();
    }

    @Override
    protected Void doInBackground(Void... params) {

        DatabaseHelper helper = new DatabaseHelper(context);
        try {
            Log.d(this.getClass().getSimpleName(), "Saving to database");
            Dao<MeasureRecord, Integer> recordDao = helper.getDao(MeasureRecord.class);
            Dao<MeasureData, Integer> dataDao = helper.getDao(MeasureData.class);
            for(List<ScanResult> oneMeasurement :listOfResults){
                if(!oneMeasurement.isEmpty()){
                    long grouId=oneMeasurement.get(0).timestamp;
                    MeasureRecord record = new MeasureRecord(x,y,floor,grouId);
                    recordDao.create(record);
                    Collections.sort(oneMeasurement, new Comparator<ScanResult>() {
                        @Override
                        public int compare(ScanResult lhs, ScanResult rhs) {
                            if(lhs.level<rhs.level){
                                return -1;
                            }else if(lhs.level == rhs.level){
                                return 0;
                            }else{
                                return 1;
                            }
                        }
                    });
                    int counter = 0;
                    for(ScanResult scan: oneMeasurement){
                        if(scan.SSID.contains("eduroam")) {
                            counter++;
                            MeasureData data = new MeasureData(record, scan.SSID, scan.BSSID, scan.level);
                            dataDao.create(data);
                        }
                        if(counter == Constants.maxSavedDataCount){
                            break;
                        }
                    }
                    while (counter < Constants.maxSavedDataCount){  //we need to align data in case there is not enough APs around
                        MeasureData data = new MeasureData(record, "fake", "zz:zz:zz:zz:zz:zz", 0);
                        dataDao.create(data);
                        counter++;
                    }
                }
            }
            List<MeasureRecord>debugList=recordDao.queryForAll();
            Log.d(AsyncSaveToDb.class.getSimpleName(), "done");

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;
    }


    @Override
    protected void onPostExecute(Void params){
        dialog.dismiss();
        //create fingerprint
    }
}