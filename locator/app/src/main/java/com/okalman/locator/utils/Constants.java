package com.okalman.locator.utils;

/**
 * Created by okalman on 11.3.16.
 */
public class Constants {
    public static final int  numberOfSquaresInLine=20;
    public static final int maxSavedDataCount=8;
    public static final int COLLECTING_TIMEOUT=25000; //25s
    public static final String  url = "https://locator-okalman-wifi.eu-gb.mybluemix.net/LocatingServlet";

    public static int getSquareNumber(float x, float y){
        int xPos=(int)(x/(1.0/(float)numberOfSquaresInLine));
        int yPos=(int)(y/(1.0/(float)numberOfSquaresInLine));
        int res=xPos+(yPos*numberOfSquaresInLine);
        return res;
    }
}
