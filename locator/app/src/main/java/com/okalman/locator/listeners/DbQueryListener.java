package com.okalman.locator.listeners;

import java.util.List;

/**
 * Created by okalman on 10.10.16.
 */
public interface DbQueryListener {
    void onDataReady(Object object);
}
