package com.okalman.locator.listeners;

/**
 * Created by okalman on 9.11.16.
 */
public interface LocalizationListener {
    void onResultReady(Integer result);
}
