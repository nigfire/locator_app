package com.okalman.locator;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.okalman.locator.asynctasks.AsyncExportDb;
import com.okalman.locator.asynctasks.AsyncQuery;
import com.okalman.locator.listeners.LocalizationListener;
import com.okalman.locator.utils.Constants;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.net.URL;

import rapid.decoder.BitmapDecoder;
import uk.co.senab.photoview.PhotoViewAttacher;

public class LocationActivity extends AppCompatActivity {

    File temp = null;
    URL website = null;
    ImageView imageMap=null;
    String link="http://indica.mendelu.cz/mapa-budovy/mistnost-Q1.20/velikost-900.png";
    PhotoViewAttacher photoViewAttacher=null;
    Bitmap bm=null;
    LocalizationListener localizationListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        imageMap = (ImageView)findViewById(R.id.image_map);
        Toolbar myToolbar = (Toolbar) findViewById(R.id.toolbar);

        try {
            website = new URL(link);
            temp = File.createTempFile("prefix", ".png", this.getCacheDir());

            new AsyncTask<Object, Void, LocationActivity>() {
                @Override
                protected LocationActivity doInBackground(Object... params) {
                    try {
                        FileUtils.copyURLToFile((URL) params[0], (File) params[1]);
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                    Log.d(this.getClass().getSimpleName(), "image loaded");
                    return (LocationActivity)params[2];
                }
                @Override
                protected void onPostExecute(LocationActivity activty){
                  Canvas canvas =  activty.setImage();
                }

            }.execute(new Object[]{website,temp,this});


        }catch (Exception e){
            e.printStackTrace();
        }
        localizationListener = new LocalizationListener() {
            @Override
            public void onResultReady(Integer result) {
                if(result!=null){
                    Canvas canvas =  LocationActivity.this.setImage();
                    LocationActivity.this.setOverlay(canvas, result);
                }else{
                    Toast.makeText(LocationActivity.this,"Localization failed, try again",Toast.LENGTH_SHORT).show();
                }
            }
        };
    }

    public Canvas setImage(){
        imageMap.setImageResource(0);

        Log.d(this.getClass().getSimpleName(), "setting image");
        if(bm==null) {
            bm = BitmapDecoder.from(temp.getAbsolutePath()).mutable().decode();
        }
        Bitmap empty = Bitmap.createBitmap(bm.getWidth(), bm.getHeight(), Bitmap.Config.ARGB_8888);
        empty.setHasAlpha(true);
        Canvas canvas = new Canvas(empty);
        canvas.drawBitmap(bm, new Matrix(), null);


        imageMap.setImageBitmap(empty);
        photoViewAttacher = new PhotoViewAttacher(imageMap);
        return canvas;
    }
    public void setOverlay(Canvas canvas, int tile){
        int xPos = (tile % Constants.numberOfSquaresInLine)*(bm.getWidth()/Constants.numberOfSquaresInLine);
        int yPos = (int)(tile/((float)Constants.numberOfSquaresInLine))*(bm.getWidth()/Constants.numberOfSquaresInLine);
        canvas.drawBitmap(createOverlay(bm), xPos, yPos, null);
    }

    Bitmap createOverlay(Bitmap bm){
        Bitmap bmOverlay = Bitmap.createBitmap(bm.getWidth()/Constants.numberOfSquaresInLine, bm.getHeight()/Constants.numberOfSquaresInLine, Bitmap.Config.ARGB_8888);
        bmOverlay.setHasAlpha(true);
        int[] pixels = new int[bmOverlay.getWidth() * bmOverlay.getHeight()];
        for(int i=0; i<pixels.length; i++){
            pixels[i]=0xFFFF0000;
        }
        bmOverlay.setPixels(pixels, 0, bmOverlay.getWidth(), 0, 0, bmOverlay.getWidth(), bmOverlay.getHeight());
        return bmOverlay;
    }

    private Canvas reloadImage(){
        photoViewAttacher.cleanup();
        imageMap.setImageResource(0);
        imageMap.invalidate();
        imageMap.refreshDrawableState();
        return setImage();
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch(item.getTitle().toString()){
            case "Refresh":{
                AsyncQuery query = new AsyncQuery(this, localizationListener);
                query.execute();
            }
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }










}
