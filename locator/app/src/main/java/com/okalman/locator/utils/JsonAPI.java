package com.okalman.locator.utils;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.okalman.locator.R;
import com.okalman.locator.db.MeasureData;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by okalman on 20.3.16.
 */
@JsonIgnoreProperties(ignoreUnknown=true)
public class JsonAPI {

    @JsonProperty
    public String key = "=$rQXue!hMjyJX54";


    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    public List<List<MeasureData>>data=new ArrayList<>();

    public String response="";

    public JsonAPI(){

    }

}
