package com.okalman.locator.asynctasks;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.widget.Toast;

import com.okalman.locator.R;
import com.okalman.locator.listeners.DbQueryListener;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by okalman on 9.3.16.
 */
public class AsyncFingerprint extends AsyncTask<Void, Long, List<List<ScanResult>>> {
    float x;
    float y;
    int floor;
    ProgressDialog dialog;
    Context context;
    int collectingTimeout;
    DbQueryListener listener;

    public AsyncFingerprint(float x, float y, int floor, Context context, int collectingTimeout, DbQueryListener listener){
        this(x,y,floor,context,collectingTimeout);
        this.listener=listener;
    }
    public AsyncFingerprint(float x, float y, int floor, Context context, int collectingTimeout){
        this.x=x;
        this.y=y;
        this.floor = floor;
        this.context=context;
        dialog = new ProgressDialog(context);
        this.collectingTimeout=collectingTimeout;
    }

    @Override
    protected void onPreExecute(){
        dialog.setMax(collectingTimeout);
        dialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setMessage(context.getString(R.string.scan_dialog));
        dialog.show();
    }

    @Override
    protected List<List<ScanResult>> doInBackground(Void... params) {
        List<List<ScanResult>> results = new ArrayList<>();
        WifiManager mainWifi = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        mainWifi.startScan();
        List<ScanResult> lastSuccessfulScan= mainWifi.getScanResults();
        long startTime= System.currentTimeMillis();
        long runningTime=0;
        while (runningTime < collectingTimeout){
            runningTime=System.currentTimeMillis()-startTime;
            publishProgress(new Long[]{runningTime});
            mainWifi.startScan();
            List<ScanResult> newScanResult= mainWifi.getScanResults();
            if(newScanResult!=null && lastSuccessfulScan !=null && !newScanResult.isEmpty() && !lastSuccessfulScan.isEmpty()){
                if(newScanResult.get(0).timestamp!=lastSuccessfulScan.get(0).timestamp){
                    lastSuccessfulScan=newScanResult;
                    results.add(newScanResult);
                }
            }
            try {
                Thread.sleep(100);
            }catch (InterruptedException e){
                Log.e(this.getClass().getSimpleName(), "Interrupted ", e);
            }
        }
        return results;
    }

    @Override
    protected void onProgressUpdate(Long... longs){
        dialog.setProgress(longs[0].intValue());
    }

    @Override
    protected void onPostExecute(final List<List<ScanResult>> results){
        if(results.isEmpty()){
            Toast.makeText(context, context.getString(R.string.scan_failure), Toast.LENGTH_SHORT).show();
        }else{
            Log.d(this.getClass().getSimpleName(), "Number of collected results: " + results.size());
        }

        dialog.dismiss();
        if(!results.isEmpty()){
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setTitle(R.string.scan_save_alert_dialog);
            builder.setCancelable(false);
            builder.setMessage("Save?");

            builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {

                public void onClick(DialogInterface dialog, int which) {
                    new AsyncSaveToDb(x,y,floor,context,results).execute();
                    new AsyncQueryDBMeasuredRecords(context,listener).execute();
                    dialog.dismiss();
                }

            });

            builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {
                    // Do nothing
                    dialog.dismiss();
                }
            });

            AlertDialog alert = builder.create();
            alert.show();
        }

    }
}