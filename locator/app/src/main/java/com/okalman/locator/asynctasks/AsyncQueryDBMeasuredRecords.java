package com.okalman.locator.asynctasks;

import android.content.Context;
import android.net.wifi.ScanResult;
import android.os.AsyncTask;

import com.j256.ormlite.dao.Dao;
import com.okalman.locator.db.DatabaseHelper;
import com.okalman.locator.db.MeasureRecord;
import com.okalman.locator.listeners.DbQueryListener;

import java.util.List;

/**
 * Created by okalman on 10.10.16.
 */
public class AsyncQueryDBMeasuredRecords extends AsyncTask<Void, Void, List<MeasureRecord>> {

    DbQueryListener listener;
    Context context;
    public AsyncQueryDBMeasuredRecords(Context context,DbQueryListener listener){
        this.listener = listener;
        this.context = context;
    }

    @Override
    protected List<MeasureRecord> doInBackground(Void... params) {
        DatabaseHelper helper = new DatabaseHelper(context);
        try {
            Dao<MeasureRecord, Integer> recordDao = helper.getDao(MeasureRecord.class);
            return recordDao.queryForAll();
        }catch(Exception e){
            return null;
        }
    }

    @Override
    protected void onPostExecute(List<MeasureRecord> records){
        if(listener != null) listener.onDataReady(records);
    }

}
