package com.okalman.locator.db;


/**
 * Created by okalman on 9.3.16.
 */

import com.j256.ormlite.dao.ForeignCollection;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;
import com.okalman.locator.utils.Constants;

@DatabaseTable
public class MeasureRecord {

    //x and y are percentage values between 0 and 1 from top left corner of map
    @DatabaseField
    float x;
    @DatabaseField
    float y;
    @DatabaseField(id = true)
    long groupId;
    @DatabaseField
    int floor;
    @DatabaseField
    int tile;

    @ForeignCollectionField
    private ForeignCollection<MeasureData> data;


    public MeasureRecord(){

    }

    public MeasureRecord(float x, float y, int floor, long groupId){
        this.x=x;
        this.y=y;
        this.groupId=groupId;
        this.floor=floor;
        this.tile= Constants.getSquareNumber(x,y);
    }

    public float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
    }

    public float getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
    }

    public long getGroupId() {
        return groupId;
    }

    public void setGroupId(long groupId) {
        this.groupId = groupId;
    }

    public int getFloor() {
        return floor;
    }

    public void setFloor(int floor) {
        this.floor = floor;
    }

    public ForeignCollection<MeasureData> getData() {
        return data;
    }

    public int getTile() {
        return tile;
    }
}
