package com.okalman.locator.asynctasks;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;

import com.j256.ormlite.dao.Dao;
import com.okalman.locator.R;
import com.okalman.locator.db.DatabaseHelper;
import com.okalman.locator.db.MeasureData;
import com.okalman.locator.db.MeasureRecord;
import com.okalman.locator.utils.Constants;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import au.com.bytecode.opencsv.CSVWriter;

/**
 * Created by okalman on 12.3.16.
 */
public class AsyncExportDb extends AsyncTask<Void, Integer, Void> {

    Context context;
    ProgressDialog dialog;
    public AsyncExportDb(Context context){
        this.context=context;
        dialog = new ProgressDialog(context);
    }


    @Override
    protected void onPreExecute(){
        dialog.setMax(100);
        dialog.setCancelable(false);
        dialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setMessage(context.getString(R.string.export_to_file_dialog));
        dialog.show();
    }

    @Override
    protected Void doInBackground(Void... params) {
        try {
            DatabaseHelper helper = new DatabaseHelper(context);
            Dao<MeasureRecord, Integer> recordDao = helper.getDao(MeasureRecord.class);
            File path = Environment.getExternalStorageDirectory();
            File file = new File(path, "exported-db");
            Log.d(AsyncExportDb.class.getSimpleName(), "Exporting to " + file.getAbsolutePath());
            if(file.exists() && file.canWrite()){
                file.delete();
            }
            ArrayList<String>headerList = new ArrayList<>();
            headerList.add("x");
            headerList.add("y");
            headerList.add("floor");
            headerList.add("tile");
            for(int i =0 ; i < Constants.maxSavedDataCount; i++){
                headerList.add("MAC_"+i);
                headerList.add("level_"+i);
            }
            CSVWriter writer = new CSVWriter(new FileWriter(file.getAbsolutePath()),';');
            writer.writeNext(headerList.toArray(new String[0]));
            Log.d("WRITING FILE:", headerList.toString());
            int totalCount = recordDao.queryForAll().size();
            int counterRecords=0;
            for(MeasureRecord record:recordDao.queryForAll()){
                ArrayList<String> csvLine = new ArrayList<>();
                csvLine.add(String.valueOf(record.getX()));
                csvLine.add(String.valueOf(record.getY()));
                csvLine.add(String.valueOf(record.getFloor()));
                csvLine.add(String.valueOf(record.getTile()));

                List<MeasureData>listOfData=new ArrayList<>();
                for(MeasureData data:record.getData()){
                    listOfData.add(data);
                }
                Collections.sort(listOfData, new Comparator<MeasureData>() {
                    @Override
                    public int compare(MeasureData lhs, MeasureData rhs) {
                        return lhs.getMac().compareTo(rhs.getMac());
                    }
                });
                int counter=0;
                for(MeasureData data:listOfData){

                    csvLine.add(data.getMac());
                    csvLine.add(String.valueOf(data.getLevel()));
                    if(counter==Constants.maxSavedDataCount-1){
                        break;
                    }
                    counter++;
                }
                writer.writeNext(csvLine.toArray(new String[0]));
                counterRecords++;
                publishProgress((int)(((double)counterRecords/(double)totalCount)*100.0));
                Log.d("WRITING FILE:", csvLine.toString());
            }
            writer.close();
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onProgressUpdate(Integer... count){
        dialog.setProgress(count[0]);
    }

    @Override
    protected void onPostExecute(Void params){
        dialog.dismiss();
        //create fingerprint
    }
}
