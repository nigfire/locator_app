package com.okalman.locator.db;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.j256.ormlite.dao.ForeignCollection;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by okalman on 11.3.16.
 */
@JsonIgnoreProperties(ignoreUnknown=true)
@DatabaseTable
public class MeasureData {
    public static final String RECORD_ID="record_id";

    @DatabaseField(generatedId = true)
    private int id;

    //x and y are percentage values between 0 and 1 from top left corner of map
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    @DatabaseField
    private String mac;
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    @DatabaseField
    private String ssid;
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    @DatabaseField
    private int level;

    @DatabaseField(foreign = true, foreignAutoRefresh = true, columnName = RECORD_ID)
    private MeasureRecord record;


    public MeasureData(){

    }

    public MeasureData(MeasureRecord record,String ssid, String mac, int level){
        this.ssid=ssid;
        this.mac=mac;
        this.level=level;
        this.record=record;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    public String getMac() {
        return mac;
    }

    public void setMac(String mac) {
        this.mac = mac;
    }

    public String getSsid() {
        return ssid;
    }

    public void setSsid(String ssid) {
        this.ssid = ssid;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }
    public MeasureRecord getRecord() {
        return record;
    }

    public void setRecord(MeasureRecord record) {
        this.record = record;
    }

}
