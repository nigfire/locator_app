package wasdev.sample.servlet.api;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

/**
 * Created by okalman on 30.3.16.
 */
public class JsonAPI {

    @JsonProperty
    private  String key;

    @JsonProperty
    private  ArrayList<ArrayList<MeasuredData>> data = new ArrayList();

    @JsonProperty
    private String response="";


    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public ArrayList<ArrayList<MeasuredData>> getData() {
        return data;
    }

    public void setData(ArrayList<ArrayList<MeasuredData>> data) {
        this.data = data;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }
}
