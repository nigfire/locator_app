package wasdev.sample.servlet;

import com.fasterxml.jackson.databind.ObjectMapper;
import wasdev.sample.servlet.api.JsonAPI;
import wasdev.sample.servlet.api.MeasuredData;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by okalman on 13.3.16.
 */
@WebServlet("/LocatingServlet")
public class LocatingServlet extends HttpServlet {
    private final static Logger LOGGER = Logger.getLogger(LocatingServlet.class.getName());

    @Override
    protected  void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request,response);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");


        StringBuffer jb = new StringBuffer();
        String line = null;
        try {
            BufferedReader reader = request.getReader();
            while ((line = reader.readLine()) != null)
                jb.append(line);


        ObjectMapper mapper = new ObjectMapper();
            LOGGER.log(Level.INFO,jb.toString());
        JsonAPI api = mapper.readValue(jb.toString(),JsonAPI.class);
            api.setResponse(new RequestRouter().sendMessage(api));
            response.setStatus(200);

        response.getWriter().print(mapper.writeValueAsString(api));
        } catch (Exception e) {
            e.printStackTrace();
            LOGGER.log(Level.SEVERE,e.getMessage());
            response.setStatus(200);
            response.getWriter().print(e.getMessage());

        }
    }
}
