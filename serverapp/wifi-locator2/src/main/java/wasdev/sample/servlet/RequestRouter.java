package wasdev.sample.servlet;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.io.IOUtils;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.*;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpHead;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicHeader;
import org.apache.http.message.BasicNameValuePair;
import wasdev.sample.servlet.api.JsonAPI;
import wasdev.sample.servlet.api.JsonRequest;
import wasdev.sample.servlet.api.MeasuredData;


import java.io.InputStream;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.sql.*;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by okalman on 13.3.16.
 */
public class RequestRouter {
    private final static Logger LOGGER = Logger.getLogger(RequestRouter.class.getName());
    private ArrayList<MeasuredData>dataList = new ArrayList<>();
    public String sendMessage(JsonAPI request) {
        try {
            if(!request.getKey().equals(Constants.KEY)){
                return "null";
            }
            JsonRequest requestForWatson= new JsonRequest();
            requestForWatson.setHeader(getHeaderFromDB());
            for(ArrayList<MeasuredData>ldata : request.getData()){


                Collections.sort(ldata, new Comparator<MeasuredData>() {
                    @Override
                    public int compare(MeasuredData lhs, MeasuredData rhs) {
                        if(lhs.getLevel()<rhs.getLevel()){
                            return -1;
                        }else if(lhs.getLevel() == rhs.getLevel()){
                            return 0;
                        }else{
                            return 1;
                        }
                    }
                });
                requestForWatson.addData(ldata);
            }
            LOGGER.log(Level.INFO, "Sending message");

            CloseableHttpClient httpclient = HttpClients.createDefault();
            HttpPost httpPost = new HttpPost("https://palblyp.pmservice.ibmcloud.com/pm/v1/score/locator?accesskey=" + Constants.KEY);
            List<NameValuePair> nvps = new ArrayList<NameValuePair>();
            BasicHeader header= new BasicHeader("Content-type","application/json;charset=UTF-8");
            httpPost.setHeader(header);
            ObjectMapper mapper = new ObjectMapper();
            LOGGER.log(Level.INFO, "Json: "+mapper.writeValueAsString(requestForWatson));
            StringEntity entity = new StringEntity(mapper.writeValueAsString(requestForWatson));
           // StringEntity entity = new StringEntity(Constants.example);
            httpPost.setEntity(entity);
            LOGGER.log(Level.INFO, "Message sent");
            HttpResponse response =httpclient.execute(httpPost);
            InputStream is=response.getEntity().getContent();
            StringWriter writer = new StringWriter();
            IOUtils.copy(is, writer, "UTF-8");
            String theString = writer.toString();
            LOGGER.log(Level.INFO, "Response: "+response.toString());
            LOGGER.log(Level.INFO, "Content "+theString);
            JsonRequest[] api = mapper.readValue(theString,JsonRequest[].class);
            request.setResponse(String.valueOf(getResult(api[0])));
            return String.valueOf(getResult(api[0]));
        }catch (Exception e){
            e.printStackTrace();
        }
        return "null";
    }

    private String[] getHeaderFromDB(){
        try {
            LOGGER.log(Level.INFO, "Obtaining header from database");
            Class.forName("com.mysql.jdbc.Driver");
            Connection connection = DriverManager.getConnection(Constants.DB_URL_REMOTE, Constants.DB_REMOTE_USER, Constants.DB_REMOTE_PASSWORD);
            Statement statement = connection.createStatement();
            String command= "SHOW COLUMNS FROM "+Constants.BIG_TABLE;
             ResultSet rs= statement.executeQuery(command);
            ArrayList<String>columns= new ArrayList<>();


            while (rs.next()) { columns.add(rs.getString("Field")); }
            LOGGER.log(Level.INFO, "Headers successfully obtained");
            return columns.toArray(new String[0]);
        }catch (Exception e){
            LOGGER.log(Level.INFO, "Failed to obtain headers");
            e.printStackTrace();
        }
        return null;

    }
    private double getResult(JsonRequest api){
        HashMap<Double, Integer> votes = new HashMap<>();
        for(Double[]array : api.data){
            for(int i=0; i<api.getHeader().length; i++){
                if(api.getHeader()[i].equals("$KNN-tile") || api.getHeader()[i].equals("$C-tile")){
                    if(votes.containsKey(Double.valueOf(array[i]))){
                        votes.put(Double.valueOf(array[i]), votes.get(Double.valueOf(array[i]))+1);
                    }else{
                        votes.put(Double.valueOf(array[i]),1);
                    }
                }
            }
        }
        Iterator it = votes.entrySet().iterator();
        Integer biggest=-1;
        Double tile=0.0;
        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry)it.next();
            LOGGER.log(Level.INFO, "Tile"+ (Double)pair.getKey() +"  has " +(Integer)pair.getValue() + " votes.");
            if((Integer)pair.getValue()>biggest){
                biggest=(Integer)pair.getValue();
                tile=(Double)pair.getKey();
            }
        }
        LOGGER.log(Level.INFO, "Returning tile: "+ tile +" with " + biggest +"votes");
        return tile;

    }

}
