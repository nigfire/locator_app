package wasdev.sample.servlet.api;

import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by okalman on 30.3.16.
 */
public class MeasuredData {

    @JsonProperty
    private String mac;
    @JsonProperty
    private String ssid;
    @JsonProperty
    private int level;

    @JsonAnySetter
    public void handleUnknown(String key, Object value) {
        return; // ignore unknown
    }

    public MeasuredData(){

    }

    public String getMac() {
        return mac;
    }

    public void setMac(String mac) {
        this.mac = mac;
    }

    public String getSsid() {
        return ssid;
    }

    public void setSsid(String ssid) {
        this.ssid = ssid;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }
}
