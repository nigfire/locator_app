package wasdev.sample.servlet.api;

import com.fasterxml.jackson.annotation.JsonProperty;
import wasdev.sample.servlet.RequestRouter;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by okalman on 30.3.16.
 */
public class JsonRequest {

    private final static Logger LOGGER = Logger.getLogger(JsonRequest.class.getName());

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    public String tablename = "scoreInput";

    @JsonProperty
    private String[] header = {"x","y","floor","tile","MAC_0", "level_0", "MAC_1", "level_1", "MAC_2", "level_2", "MAC_3", "level_3", "MAC_4", "level_4", "MAC_5", "level_5", "MAC_6", "level_6", "MAC_7", "level_7"};
    @JsonProperty
    public List<Double[]> data=new ArrayList<>();

    private ArrayList<String> defaultRow= new ArrayList<>();
    public  JsonRequest(){

    }

    public void addData(ArrayList<MeasuredData> dataRow){
        if(header==null){
            LOGGER.log(Level.WARNING, "No header was set");
            return;
        }
        Double[]row= new Double[header.length];
        for(int i=0; i<row.length; i++){
            row[i]=-200.0;
        }
        int counter=0;
        for(MeasuredData record: dataRow){
            if(record.getSsid().toLowerCase().equals("eduroam")){
                String mac="_"+record.getMac().replaceAll(":","");
                for(int i=0; i<header.length; i++){
                    if(header[i].equals(mac)){
                        row[i]=Double.valueOf(record.getLevel());
                        counter++;
                        break;
                    }
                }
                if(counter==8) break;
            }

        }
        data.add(row);
    }
    public void setHeader(String[] header) {
        this.header = header;

    }

    public String[] getHeader() {
        return header;
    }


}
